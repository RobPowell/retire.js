package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const pathOutput = "retire.json"

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	cmd := exec.Command("retire", "--outputformat", "json", "--outputpath", pathOutput, "--exitwith", "0")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathOutput))
}
