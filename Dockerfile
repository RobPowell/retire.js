FROM node:11-alpine

ARG RETIRE_JS_VERSION
ENV RETIRE_JS_VERSION ${RETIRE_JS_VERSION:-1.6.2}
RUN npm install -g retire@$RETIRE_JS_VERSION

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
