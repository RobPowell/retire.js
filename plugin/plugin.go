package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	switch info.Name() {
	case "package.json":
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("retire.js", Match)
}
