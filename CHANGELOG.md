# Retire.js analyzer changelog

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Bump RetireJs to 1.6.2

## v1.1.1
- Fix empty `location.file` field, use `package.json` as a default

## v1.1.0
- Add dependency (package name and version) to report
- Improve vulnerability name, message and compare key

## v1.0.0
- Initial release
